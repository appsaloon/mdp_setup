<?php
// ======================================================
// MDP 3.0 wp-config + .env
// ======================================================

require_once(__DIR__ . '/vendor/autoload.php');
(\Dotenv\Dotenv::create( __DIR__ . '/' ))->load();

ini_set( 'display_errors', 0 );

// =======================================================
// Load database info and development parameters from .env
// =======================================================

// $envs is needed for the wp-stage-switcher plugin
$envs = [
    'local'       => getenv('URL_LOCAL'),
    'staging'     => getenv('URL_STAGING'),
    'production'  => getenv('URL_PRODUCTION')
];

define('WP_ENV', getenv('ENVIRONMENT'));
define('ENVIRONMENTS', $envs );
define('DB_NAME', getenv('DB_NAME'));
define('DB_USER', getenv('DB_USER'));
define('DB_PASSWORD', getenv('DB_PASSWORD'));
define('DB_HOST', getenv('DB_HOST'));
define('WP_HOME', getenv('WP_HOME'));
define('WP_SITEURL', getenv('WP_HOME').'/wp');
define('WP_DEBUG', getenv('WP_DEBUG') );
define('WP_DEBUG_DISPLAY', getenv('WP_DEBUG_DISPLAY') );
define('WP_DEBUG_LOG', getenv('WP_DEBUG_LOG') );
define('DISALLOW_FILE_MODS', getenv('DISALLOW_FILE_MODS') );
define( 'DISALLOW_FILE_EDIT', getenv('DISALLOW_FILE_EDIT') );
define('AUTOMATIC_UPDATER_DISABLED', getenv('AUTOMATIC_UPDATER_DISABLED') );

// ======================================================
// Spinupwp settings for cache
// ======================================================
define( 'WP_CACHE_KEY_SALT', getenv('DB_HOST') );
define( 'WP_REDIS_SELECTIVE_FLUSH', true );
define( 'SPINUPWP_CACHE_PATH', '/cache/' . getenv('DB_HOST') );

// ======================================================
// You almost certainly do not want to change these
// ======================================================
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

// ======================================================
// Load WordPress salt https://api.wordpress.org/secret-key/1.1/salt/
// ======================================================
define('AUTH_KEY',         '');
define('SECURE_AUTH_KEY',  '');
define('LOGGED_IN_KEY',    '');
define('NONCE_KEY',        '');
define('AUTH_SALT',        '');
define('SECURE_AUTH_SALT', '');
define('LOGGED_IN_SALT',   '');
define('NONCE_SALT',       '');

// ========================
// Custom Content Directory
// ========================
define('WP_CONTENT_URL', WP_HOME .  '/wp-content' );
define('WP_CONTENT_DIR', dirname( ABSPATH ) . '/wp-content' );

// =====================================================
// Load WordPress Settings - please change table prefix
// =====================================================
$table_prefix  = 'wp_';
define('WP_POST_REVISIONS', 3);
define('DISABLE_WP_CRON', true );

// ====================================================
// Inserted by Local by Flywheel
// ====================================================
if (isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ) {
	$_SERVER['HTTPS'] = 'on';
}

// ====================================================
// Absolute path to the WordPress directory
// ====================================================
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/wp' );

// ====================================================
// Sets up WordPress vars and included files
// ====================================================
require_once ABSPATH . 'wp-settings.php';
