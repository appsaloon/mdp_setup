#### Project_name ####
Trello => url

#### GIT remote locations ####
* git remote add staging ssh://projectname@web-00(server-number).appsaloon.openminds.be/home/project_name/git/project_name.git
* git remote add master ssh://projectname@web-00(server-number).appsaloon.openminds.be/home/project_name/git/project_name.git


#### How do I setup? #####
https://appsaloon.atlassian.net/wiki/spaces/DOC/pages/204734528/Project+Setup

#### Start script ####
To install composer, & execute composer
open up SSH to local server, cd to `app/public` & run `./start.sh`

https://bitbucket.org/appsaloon/boilerplate-start/src/master/

#### Wordpress Core translation ####
Select your language here

https://wp-languages.github.io/

#### Domains xxxxxx ####
* Live/Production => URL
* Staging => URL
* Local => URL

#### SPOC ####
* team - wpdev@appsaloon.be
* you - developer - you@appsaloon.be
* PM - Pm@appsaloon.be
* client - Client - client@client.com

#### Changelog ####
* 25-10-2018 => 
	* removed Bootstrap 3 in favor of Bootstrap 4
	* removed Bootstrap javascript's & partials folders
	* added vendor folder & readme.txt for librarys
	* updated composer for Appsaloon default plugins
	* prepared composer for .env future way of work
* previous => initial